import bcrypt from "bcrypt-nodejs";
import { NextFunction, Request, Response } from "express";
import * as jwt from "jsonwebtoken";
import passport from "passport";
import { User } from "../models/user";
import { JWT_SECRET } from "../utils/secrets";


export class UserController {

  public async register(req: Request, res: Response): Promise<void> {
    const hashedPassword = bcrypt.hashSync(req.body.password, bcrypt.genSaltSync(10));

    await User.create({
      username: req.body.username,
      password: hashedPassword,

    });

    const token = jwt.sign({username: req.body.username}, JWT_SECRET);
    res.status(200).send({username: req.body.username, access_token: token});
  }

  public login(req: Request, res: Response, next: NextFunction) {
    passport.authenticate("local", function (err, user, info) {
      if (err) return next(err);
      if (!user) {
        return res.status(401).json({status: "error", code: "unauthorized"});
      } else {
        const token = jwt.sign({username: user.username}, JWT_SECRET);
        res.status(200).send({access_token: token});
      }
    });
  }

}