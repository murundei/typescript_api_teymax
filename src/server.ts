import express from "express";
import mongoose from "mongoose";

import cors from "cors";

import { MONGODB_URI } from "./utils/secrets";

import { UserRoutes } from "./routes/userRoutes";
import { ProductRoutes } from "./routes/productRoutes";


class Server {
  public app: express.Application;

  constructor() {
    this.app = express();
    this.config();
    this.routes();
    this.mongo();
  }

  public routes(): void {
    this.app.use("/api/user", new UserRoutes().router);
    this.app.use("/api/products", new ProductRoutes().router);
  }

  public config(): void {
    this.app.set("port", process.env.PORT || 9000);
    this.app.use(express.json());
    this.app.use(express.urlencoded({extended: false}));
    this.app.use(cors());
  }

  private mongo() {
    const connection = mongoose.connection;
    connection.on("connected", () => {
      console.log("DB connection established!");
    });
    connection.on("reconnected", () => {
      console.log("DB connection reestablished!");
    });
    connection.on("disconnected", () => {
      console.log("DB connection lost... Trying to reconnect...");
      setTimeout(() => {
        mongoose.connect(MONGODB_URI, {
          autoReconnect: true, keepAlive: true,
          socketTimeoutMS: 3000, connectTimeoutMS: 3000
        });
      }, 3000);
    });
    connection.on("close", () => {
      console.log("DB connection closed.");
    });
    connection.on("error", (error: Error) => {
      console.log("DB connection error: " + error);
    });

    const run = async () => {
      await mongoose.connect(MONGODB_URI, {
        autoReconnect: true, keepAlive: true
      });
    };
    run().catch(error => console.log(error));
  }

  public start(): void {
    this.app.listen(this.app.get("port"), () => {
      console.log("Server is listening at http://localhost:%d", this.app.get("port"));
    });
  }

}

const server = new Server();
server.start();