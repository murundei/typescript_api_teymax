Brand new Node.JS REST API written in TypeScript.
Features:
- Base server setup with beautiful MongoDB connector.
- Beautiful routes setup.
- "User" model, Passport and JsonWebToken auth along with JWT-protected routes.
- Base "Product" model along with CRUD operations with id.
- Well-done controllers for both models as well as auth controllers

Installation:
Clone the repo and run "npm i" in your terminal.